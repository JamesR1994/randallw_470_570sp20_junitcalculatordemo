package edu.uscb.csci470.calculator.tests;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.*;
// import org.junit.Test;
import edu.uscb.csci470.calculator.Calculator;


// NOTE: make sure your pom.xml file has the necessary dependencies
//   so that Eclipse knows what to import!

public class CalculatorTest {
   private Calculator calculatorTestInstance; // has class-wide scope, private to class
   private static Logger logger = LoggerFactory.getLogger( CalculatorTest.class);
   
   @BeforeClass
   public static void setUpBeforeAllTests() {
  logger.info("Starting test for this class...");
   } // end method setUpBeforeAllTests
   
   @Before
   public void setUpBeforeEachTest() {
 
  calculatorTestInstance = new Calculator();
  logger.info("new test instance: " + calculatorTestInstance);
 
 
   } // end method setUpBeforeEachTest
   
   @After
   public void teardownAfterEachTest() {
  logger.info("a test was just run...");
   } // end method teardownAfterEachTest
   
   @AfterClass
   public static void teardownAfterAllTests() {
  logger.info("All tests completed, closing DB connections etc.");
   } // end method teardownAfterAllTests
   
   @Test
   public void testAdd() {
  int value1 = 25;
  int value2 = 10;
  int expectedResult = 35;
  int testResult = calculatorTestInstance.add(value1, value2);
  Assert.assertEquals(expectedResult, testResult);
 
   } // end test method testAdd
   
   @Test
   public void testSubtract() {
  int value1 = 25;
  int value2 = 10;
  int expectedResult = 15;
int testResult = calculatorTestInstance.subtract(value1, value2);
  Assert.assertEquals(expectedResult, testResult);
   } // end test method testSubtract
   
   @Test
   public void testMultiply() {
  int value1 = 2;
  int value2 = 3;
  long expectedResult = 6;
  long testResult = calculatorTestInstance.multiply(value1, value2);
  Assert.assertEquals(expectedResult, testResult);
   } // end test method testMultiply
   
   @Test
   public void testIntDivide() {
  int value1 = 25;
  int value2 = 10;
  int expectedResult = 2;
  int testResult = calculatorTestInstance.intDivide(value1, value2);
  Assert.assertEquals(expectedResult, testResult);
   } // end test method IntDivide
   
   @ Test( expected = IllegalArgumentException.class ) 
   public void testIntDivideByZero() {
	   int value1 = 25;
	   int value2 = 0;
	   try {
		calculatorTestInstance.intDivide(value1, value2);
	} finally {
		System.out.println("TestIntDivideByZero complete");
	}// end try statement
   } // end test testIntDivideByZero method
   
   @Test
	public void testDivide() {
		int value1 = 25;
		int value2 = 10;
		double tolerance = 0.00005;
		double expectedResult = 2.5;
		double testResult = calculatorTestInstance.divide(value1, value2);
		Assert.assertEquals(expectedResult, testResult, tolerance);
	} // end testDivide
   
} // end class CalculatorTest


