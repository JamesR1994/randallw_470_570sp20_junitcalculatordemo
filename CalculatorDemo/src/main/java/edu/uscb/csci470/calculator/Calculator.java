package edu.uscb.csci470.calculator;

/**
 * Calculator class that implements basic 
 * arithmetic operations as named methods
 * 
 * @author randallw@email.uscb.edu
 * @version 0.1
 */
public class Calculator {
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @return the sum of a and b 
	 */

	public int add(int a , int b) {
		return a + b;
	}// end method add;
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @return the difference of a and b 
	 */
	
	public int subtract(int a , int b) {
		return a - b;
	}// end method subtract;
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @return the multiplication of a and b 
	 */
	
	public long multiply(int a , int b) {
		return a * b;
	}// end method multiply;
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @return the division of a and b 
	 */
	
	public int intDivide(int a , int b) {
		int result;
		if (b == 0) {
			throw new IllegalArgumentException(
					"intDivide method: cannot divide by zero");
		} else {
			result = a / b;
		} // end if/else statement
		return result;
	}// end method intDivide;
	
	/**
	 * Return the floating point division result of 
	 * dividing a by b. note that this method throws and exception when b==0.
	 * @param a the dividend or numerator
	 * @param b the divisor or denominator
	 * @return the floating point quotient of a and b
	 */
	public double divide( int a , int b) {
		double result;
		
		if (b == 0) {
			throw new IllegalArgumentException(
					"Divide method: cannot divide by zero");
		} else {
			result = (double)a / (double)b;
		} // end if/else statement
		return result;
	} // end method divide
	
} // end class Calculator
